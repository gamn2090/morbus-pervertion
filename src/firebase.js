import firebase from "firebase/app"
import "firebase/database"
import "firebase/auth"
import "firebase/storage"

const config = {
    apiKey: "AIzaSyDsEBkalmOO43MVYA35ZgAhDW_OW4fCz6o",
    authDomain: "mobius-page.firebaseapp.com",
    databaseURL: "https://mobius-page.firebaseio.com",
    projectId: "mobius-page",
    storageBucket: "",
    messagingSenderId: "1056883907365",
}

firebase.initializeApp(config)

export const db = firebase.database().ref()
export const auth = firebase.auth()
export const storage = firebase.storage()