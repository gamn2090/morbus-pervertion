// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
//importas y usas firebase
import './firebase'
import { auth } from '@/firebase.js'

import VModal from 'vue-js-modal'
Vue.use(VModal)

import VueRouter from 'vue-router'
Vue.use(VueRouter);

import { routes } from './router/index'

Vue.config.productionTip = false
/*instanciamos las rutas*/
const router = new VueRouter({
  mode: "history",
  routes: routes,
  scrollBehavior(to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
});

router.beforeEach((to, from, next) => {
  let usuario = auth.currentUser;
  //console.log(usuario);
  let autorizacion = to.matched.some(record => record.meta.autentificado);

  if (autorizacion && !usuario) {

    next('Home')

  } else if (!autorizacion && usuario) {

    next('Dashboard');

  } else {

    next();

  }
})

auth.onAuthStateChanged(function (user) {

  new Vue({
    el: '#app',
    router,
    render: h => h(App)
  })

});