import Home from '@/components/pages/Home'
import Login from '@/Auth/Login'
import Dashboard from "@/components/pages/Dashboard.vue";
import Profile from "@/components/Admin/profile.vue";


export const routes = [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      //pantalla de login
      path: "/login",
      name: "Login",
      component: Login
    },
    {
      //dashboard del administrador **protegido**
      path: "/dashboard",
      name: "Dashboard",
      component: Dashboard,
      meta: {
        autentificado: true
      },
      children: [
        {
          path: "/",
          redirect: "profile",
          component: Profile
        },
        {
          path: "profile",
          component: Profile
        },
      ]
    }
  ]

